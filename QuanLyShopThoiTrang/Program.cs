﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyShopThoiTrang.Models;

namespace QuanLyShopThoiTrang
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      using (var db = new DBContexts.QLSTTDB())
      {
        var queryAdmin = from s in db.Staffs where s.Username.Equals("admin") select s;
        if (queryAdmin.Count() == 0)
        {
          Staff admin = new Staff
          {
            Username = "admin",
            Password = Utils.Hash("111111"),
            Name = "Quản lý"
          };

          db.Staffs.Add(admin);
          db.SaveChanges();
        }
      }
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new FormMain());
    }
  }
}
