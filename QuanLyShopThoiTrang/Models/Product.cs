﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyShopThoiTrang.Models
{
  public enum Type
  {
    A,
    B
  }

  public class Product
  {
   
    public int Id { get; set; }
    public string SKU { get; set; }
    public string Name { get; set; }
    public Type Type { get; set; }
  }
}
