﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QuanLyShopThoiTrang.Models
{
  public enum Role
  {
    A, // Chủ shop
    B, // Nhân viên kho
    C  // Nhân viên bán hàng
  }
  public class Staff
  {
    public int Id { get; set; }
    [Required]
    public string Name { get; set; }
    public DateTime? Birthday { get; set; }
    public string Address { get; set; }
    [Required]
    public string Username { get; set; }
    [Required]
    public string Password { get; set; }
    [Required]
    public Role Role { get; set; }
  }
}
