﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Linq;

namespace QuanLyShopThoiTrang
{
  public partial class FormLogin : Form
  {
    public bool isLoggedIn
    {
      get;
      set;
    }
    private Rectangle cRect = new Rectangle(-1, -1, -1, -1);

    public FormLogin()
    {
      InitializeComponent();
    }

    private void FormLogin_Load(object sender, EventArgs e)
    {

    }
    
    protected override void OnPaintBackground(PaintEventArgs e)
    {
      Color from = Utils.Color("#e0c3fc");
      Color to = Utils.Color("#8ec5fc");
      if (cRect.Height == -1)
      {
        cRect = ClientRectangle;
      }
      LinearGradientBrush brush = new LinearGradientBrush(cRect,
                                                                 from,
                                                                 to,
                                                                 0F);
      try
      {
          e.Graphics.FillRectangle(brush, cRect);
      }
      catch (ArgumentException)
      {

      }
      
    }

    private void textBox1_TextChanged(object sender, EventArgs e)
    {

    }

    private void label1_Click(object sender, EventArgs e)
    {

    }

    private void btnThoat_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }

    private void btnDangNhap_Click(object sender, EventArgs e)
    {
      if (txtTenDangNhap.Text == "")
      {
        errorProvider1.SetError(txtTenDangNhap, "Tên đăng nhập không được để trống");
        return;
      }
      else
      {
        errorProvider1.Clear();
      }

      if (txtMatKhau.Text == "")
      {
        errorProvider1.SetError(txtMatKhau, "Mật khẩu không được để trống");
        return;
      }
      else
      {
        errorProvider1.Clear();
      }

      using (var db = new DBContexts.QLSTTDB())
      {
        var queryUser = from s in db.Staffs 
                        where s.Username.Equals(txtTenDangNhap.Text)
                        select s;
        if (queryUser.Count() == 0)
        {
          isLoggedIn = false;
          MessageBox.Show("Sai tên đăng nhập hoặc mật khẩu!", "Lỗi");
        }
        else
        {
          if (Utils.Verify(txtMatKhau.Text, queryUser.First().Password))
          {
            isLoggedIn = true;
            MessageBox.Show("Xin chào " + queryUser.First().Name, "Thành công");
            Close();
          }
          else
          {
            isLoggedIn = false;
            MessageBox.Show("Sai tên đăng nhập hoặc mật khẩu!", "Lỗi");
          }
        }
      }
    }
  }
}
