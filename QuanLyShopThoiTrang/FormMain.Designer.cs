﻿namespace QuanLyShopThoiTrang
{
  partial class FormMain
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tabControl1 = new System.Windows.Forms.TabControl();
      this.dashboardPage = new System.Windows.Forms.TabPage();
      this.sellPage = new System.Windows.Forms.TabPage();
      this.panel2 = new System.Windows.Forms.Panel();
      this.button6 = new System.Windows.Forms.Button();
      this.comboBox1 = new System.Windows.Forms.ComboBox();
      this.textBox2 = new System.Windows.Forms.TextBox();
      this.listBox1 = new System.Windows.Forms.ListBox();
      this.label8 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.panel1 = new System.Windows.Forms.Panel();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.listView1 = new System.Windows.Forms.ListView();
      this.button5 = new System.Windows.Forms.Button();
      this.button4 = new System.Windows.Forms.Button();
      this.button3 = new System.Windows.Forms.Button();
      this.button2 = new System.Windows.Forms.Button();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.label6 = new System.Windows.Forms.Label();
      this.button1 = new System.Windows.Forms.Button();
      this.label5 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.customerPage = new System.Windows.Forms.TabPage();
      this.khoPage = new System.Windows.Forms.TabPage();
      this.nhanvienPage = new System.Windows.Forms.TabPage();
      this.greetingLabel = new System.Windows.Forms.Label();
      this.tabControl1.SuspendLayout();
      this.sellPage.SuspendLayout();
      this.panel2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.panel1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // tabControl1
      // 
      this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.tabControl1.Controls.Add(this.dashboardPage);
      this.tabControl1.Controls.Add(this.sellPage);
      this.tabControl1.Controls.Add(this.customerPage);
      this.tabControl1.Controls.Add(this.khoPage);
      this.tabControl1.Controls.Add(this.nhanvienPage);
      this.tabControl1.Location = new System.Drawing.Point(12, 30);
      this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new System.Drawing.Size(1157, 616);
      this.tabControl1.TabIndex = 0;
      // 
      // dashboardPage
      // 
      this.dashboardPage.Location = new System.Drawing.Point(4, 26);
      this.dashboardPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.dashboardPage.Name = "dashboardPage";
      this.dashboardPage.Size = new System.Drawing.Size(1149, 586);
      this.dashboardPage.TabIndex = 2;
      this.dashboardPage.Text = "Bảng điều khiển";
      this.dashboardPage.UseVisualStyleBackColor = true;
      // 
      // sellPage
      // 
      this.sellPage.Controls.Add(this.panel2);
      this.sellPage.Controls.Add(this.panel1);
      this.sellPage.Location = new System.Drawing.Point(4, 26);
      this.sellPage.Name = "sellPage";
      this.sellPage.Size = new System.Drawing.Size(1149, 586);
      this.sellPage.TabIndex = 3;
      this.sellPage.Text = "Bán hàng";
      this.sellPage.UseVisualStyleBackColor = true;
      // 
      // panel2
      // 
      this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.panel2.BackColor = System.Drawing.Color.White;
      this.panel2.Controls.Add(this.button6);
      this.panel2.Controls.Add(this.comboBox1);
      this.panel2.Controls.Add(this.textBox2);
      this.panel2.Controls.Add(this.listBox1);
      this.panel2.Controls.Add(this.label8);
      this.panel2.Controls.Add(this.label7);
      this.panel2.Controls.Add(this.pictureBox1);
      this.panel2.Location = new System.Drawing.Point(3, 3);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(742, 580);
      this.panel2.TabIndex = 1;
      // 
      // button6
      // 
      this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.button6.Location = new System.Drawing.Point(634, 284);
      this.button6.Name = "button6";
      this.button6.Size = new System.Drawing.Size(105, 25);
      this.button6.TabIndex = 7;
      this.button6.Text = "Tìm kiếm";
      this.button6.UseVisualStyleBackColor = true;
      // 
      // comboBox1
      // 
      this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Items.AddRange(new object[] {
            "Tên sản phẩm",
            "Mã sản phẩm"});
      this.comboBox1.Location = new System.Drawing.Point(391, 284);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new System.Drawing.Size(237, 25);
      this.comboBox1.TabIndex = 6;
      this.comboBox1.Text = "Tìm theo...";
      // 
      // textBox2
      // 
      this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.textBox2.Location = new System.Drawing.Point(3, 284);
      this.textBox2.Name = "textBox2";
      this.textBox2.Size = new System.Drawing.Size(382, 25);
      this.textBox2.TabIndex = 4;
      // 
      // listBox1
      // 
      this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.listBox1.FormattingEnabled = true;
      this.listBox1.ItemHeight = 17;
      this.listBox1.Location = new System.Drawing.Point(3, 315);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new System.Drawing.Size(736, 259);
      this.listBox1.TabIndex = 3;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(237, 122);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(84, 17);
      this.label8.TabIndex = 2;
      this.label8.Text = "250.000 VNĐ";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(237, 93);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(102, 17);
      this.label7.TabIndex = 1;
      this.label7.Text = "Khăn choàng cổ";
      // 
      // pictureBox1
      // 
      this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.pictureBox1.Image = global::QuanLyShopThoiTrang.Properties.Resources.test_product;
      this.pictureBox1.Location = new System.Drawing.Point(14, 18);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(196, 213);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.pictureBox1.TabIndex = 0;
      this.pictureBox1.TabStop = false;
      // 
      // panel1
      // 
      this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.panel1.BackColor = System.Drawing.Color.White;
      this.panel1.Controls.Add(this.groupBox2);
      this.panel1.Controls.Add(this.groupBox1);
      this.panel1.Location = new System.Drawing.Point(751, 3);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(395, 580);
      this.panel1.TabIndex = 0;
      // 
      // groupBox2
      // 
      this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox2.Controls.Add(this.listView1);
      this.groupBox2.Controls.Add(this.button5);
      this.groupBox2.Controls.Add(this.button4);
      this.groupBox2.Controls.Add(this.button3);
      this.groupBox2.Controls.Add(this.button2);
      this.groupBox2.Location = new System.Drawing.Point(3, 3);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(389, 355);
      this.groupBox2.TabIndex = 10;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Sản phẩm";
      this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
      // 
      // listView1
      // 
      this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.listView1.Location = new System.Drawing.Point(6, 24);
      this.listView1.Name = "listView1";
      this.listView1.Size = new System.Drawing.Size(377, 286);
      this.listView1.TabIndex = 4;
      this.listView1.UseCompatibleStateImageBehavior = false;
      // 
      // button5
      // 
      this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.button5.Location = new System.Drawing.Point(307, 316);
      this.button5.Name = "button5";
      this.button5.Size = new System.Drawing.Size(75, 33);
      this.button5.TabIndex = 3;
      this.button5.Text = "Xóa";
      this.button5.UseVisualStyleBackColor = true;
      // 
      // button4
      // 
      this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.button4.Location = new System.Drawing.Point(200, 316);
      this.button4.Name = "button4";
      this.button4.Size = new System.Drawing.Size(103, 33);
      this.button4.TabIndex = 2;
      this.button4.Text = "Giảm số lượng";
      this.button4.UseVisualStyleBackColor = true;
      // 
      // button3
      // 
      this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.button3.Location = new System.Drawing.Point(86, 316);
      this.button3.Name = "button3";
      this.button3.Size = new System.Drawing.Size(110, 33);
      this.button3.TabIndex = 1;
      this.button3.Text = "Tăng số lượng";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new System.EventHandler(this.button3_Click);
      // 
      // button2
      // 
      this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.button2.Location = new System.Drawing.Point(7, 316);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(75, 33);
      this.button2.TabIndex = 0;
      this.button2.Text = "Thêm";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new System.EventHandler(this.button2_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox1.Controls.Add(this.textBox1);
      this.groupBox1.Controls.Add(this.label6);
      this.groupBox1.Controls.Add(this.button1);
      this.groupBox1.Controls.Add(this.label5);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Location = new System.Drawing.Point(3, 364);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(389, 213);
      this.groupBox1.TabIndex = 9;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Thanh toán";
      // 
      // textBox1
      // 
      this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.textBox1.Location = new System.Drawing.Point(169, 37);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new System.Drawing.Size(166, 25);
      this.textBox1.TabIndex = 4;
      // 
      // label6
      // 
      this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label6.ForeColor = System.Drawing.Color.Red;
      this.label6.Location = new System.Drawing.Point(212, 104);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(123, 30);
      this.label6.TabIndex = 7;
      this.label6.Text = "80.000 VNĐ";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // button1
      // 
      this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.button1.BackColor = System.Drawing.Color.Transparent;
      this.button1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.button1.Location = new System.Drawing.Point(255, 167);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(123, 36);
      this.button1.TabIndex = 0;
      this.button1.Text = "THANH TOÁN";
      this.button1.UseVisualStyleBackColor = false;
      // 
      // label5
      // 
      this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label5.ForeColor = System.Drawing.Color.Lime;
      this.label5.Location = new System.Drawing.Point(185, 67);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(150, 30);
      this.label5.TabIndex = 6;
      this.label5.Text = "6.520.000 VNĐ";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.label5.Click += new System.EventHandler(this.label5_Click);
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(97, 77);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(66, 17);
      this.label1.TabIndex = 1;
      this.label1.Text = "Tổng tiền:";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label4
      // 
      this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(341, 40);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(35, 17);
      this.label4.TabIndex = 5;
      this.label4.Text = "VNĐ";
      // 
      // label2
      // 
      this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(18, 40);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(145, 17);
      this.label2.TabIndex = 2;
      this.label2.Text = "Khách hàng thanh toán:";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label3
      // 
      this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(47, 114);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(116, 17);
      this.label3.TabIndex = 3;
      this.label3.Text = "Trả lại khách hàng:";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // customerPage
      // 
      this.customerPage.Location = new System.Drawing.Point(4, 26);
      this.customerPage.Name = "customerPage";
      this.customerPage.Size = new System.Drawing.Size(1149, 586);
      this.customerPage.TabIndex = 4;
      this.customerPage.Text = "Khách hàng";
      this.customerPage.UseVisualStyleBackColor = true;
      // 
      // khoPage
      // 
      this.khoPage.Location = new System.Drawing.Point(4, 26);
      this.khoPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.khoPage.Name = "khoPage";
      this.khoPage.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.khoPage.Size = new System.Drawing.Size(1149, 586);
      this.khoPage.TabIndex = 1;
      this.khoPage.Text = "Kho";
      this.khoPage.UseVisualStyleBackColor = true;
      // 
      // nhanvienPage
      // 
      this.nhanvienPage.Location = new System.Drawing.Point(4, 26);
      this.nhanvienPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.nhanvienPage.Name = "nhanvienPage";
      this.nhanvienPage.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.nhanvienPage.Size = new System.Drawing.Size(1149, 586);
      this.nhanvienPage.TabIndex = 0;
      this.nhanvienPage.Text = "Nhân viên";
      this.nhanvienPage.UseVisualStyleBackColor = true;
      // 
      // greetingLabel
      // 
      this.greetingLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.greetingLabel.AutoSize = true;
      this.greetingLabel.Location = new System.Drawing.Point(966, 9);
      this.greetingLabel.Name = "greetingLabel";
      this.greetingLabel.Size = new System.Drawing.Size(203, 17);
      this.greetingLabel.TabIndex = 1;
      this.greetingLabel.Text = "Chào buổi chiều, Phạm Tuấn Anh!";
      this.greetingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // FormMain
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1181, 659);
      this.Controls.Add(this.greetingLabel);
      this.Controls.Add(this.tabControl1);
      this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.Name = "FormMain";
      this.Text = "MDC Fashion - Quản trị";
      this.Load += new System.EventHandler(this.FormMain_Load);
      this.tabControl1.ResumeLayout(false);
      this.sellPage.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.panel2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.panel1.ResumeLayout(false);
      this.groupBox2.ResumeLayout(false);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage nhanvienPage;
    private System.Windows.Forms.TabPage khoPage;
    private System.Windows.Forms.TabPage dashboardPage;
    private System.Windows.Forms.Label greetingLabel;
    private System.Windows.Forms.TabPage sellPage;
    private System.Windows.Forms.TabPage customerPage;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Button button5;
    private System.Windows.Forms.Button button4;
    private System.Windows.Forms.Button button3;
    private System.Windows.Forms.Button button2;
    private System.Windows.Forms.ListView listView1;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.Button button6;
    private System.Windows.Forms.ComboBox comboBox1;
    private System.Windows.Forms.TextBox textBox2;
    private System.Windows.Forms.ListBox listBox1;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label7;
  }
}